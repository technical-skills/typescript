var message = "Cuong Nguyen";
//Cách 1: Cách thuần
var log = function (message) {
    console.log(message);
};
// Cách 2S: Sử dụng arrow function nhưng vẫn chưa gọn.
var doLog = function (message) {
    console.log(message);
};
//Cách 3: Sử dụng arrow function gọn hơn
var doLog2 = function (message) { return console.log(message); };
//Cách 3.1: Nếu hàm không có tham số thì chúng ta không càn truyền message.
var doLog3 = function () { return console.log(message); };
console.log(log);
console.log(doLog);
console.log(doLog2);
console.log(doLog3);
