class Point7{
    constructor(public x?: number, private y?: number){
    }
    draw(){
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y );
    }

    getDistance(another : Point7){

    }
}

let point55 = new Point7(1,4);
point55.x = 5; //Truy cập vào class. Khi property này là public
point55.draw();

//Khi chúng ta cần set các giá trị mà các điều kiện cần phải thỏa mãn thì mới set giá trị cho nó. 
//Nếu ở đâu chung ta set giá trị đều if else thì dư thừa và khó dùng.
//=> Giải pháp cho việc này chúng ta sử dụng getter - setter trong TS
class Point8{
    constructor(private _x?: number, private _y?: number){
    }
    
    draw(){
        // ...
        console.log('X: ' + this._x + ', Y: ' + this._y );
    }

    getDistance(another : Point8){

    }
    set x(theX : number){
        if(theX < 0)
            throw new Error("X cannot be less 0 !");
        this._x = theX;
    }

    get x(){
        return this._x!;
    }
}

let point8 = new Point8(12,44);
let x = point8.x;
console.log("Getter:" + x);
point8.draw();

/*
Trước hết, cập nhật chỉ thị truy cập của các thuộc tính age, firstName và lastName từ public thành private.
Thứ hai, thay đổi tên thuộc tính age thành _age.
Thứ ba, tạo phương thức getter và setter cho thuộc tính _age. Trong phương thức setter, hãy kiểm tra tính hợp lệ của độ tuổi đầu vào trước khi gán nó cho thuộc tính _age.
 */
