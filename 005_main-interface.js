//Thường 1 hàm sẽ như thế này.
var drawPoint = function (x, y, z, t) {
    //..do something
};
drawPoint(4, 5, 6, 7);
//Tuy nhiên nếu nhiều parameter > 3 thì nên tạo riêng ra dạng object
var drawPoint2 = function (point) {
    //..do something
};
drawPoint2({
    x: 1,
    y: 2,
    z: 3,
    t: 4
});
//Thay vì mình truyền v thì có thể tạo ra biết object. Rồi truyền vào.
var obj = {
    x: 1,
    y: 2,
    z: 3,
    t: 4
};
var drawPoint3 = function (point) {
    //..do something
};
var obj2 = {
    x: 55555,
    y: 2,
    z: 3,
    t: 4
};
drawPoint3(obj2);
//Thử xem 1 hàm có 2 interface sẽ như thế nào.
var drawPoint4 = function (pointA, pointB) {
    console.log(pointA.x);
    console.log(pointB.x);
};
var obj3 = {
    x: 555,
    y: 2,
    z: 3,
    t: 4
};
drawPoint4(obj2, obj3);
