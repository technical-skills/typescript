class Point4{
    private x: number;
    private y: number;

    constructor(x?: number, y?: number){
        this.x = x;
        this.y = y;
    }
    draw(){
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y );
    }

    getDistance(another : Point4){

    }
}
let point5: Point4 = new Point4(1,2);
point5.draw();
//Các phạm vi truy cập 
/*
    + Private: Chỉ trong class mới được sử dụng
    + Public: Ở đâu cũng sử dụng được
    + Protected: Chỉ sử khi ở class con nếu class con kế thừa từ lớp cha.
*/
