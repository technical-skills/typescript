var Point7 = /** @class */ (function () {
    function Point7(x, y) {
        this.x = x;
        this.y = y;
    }
    Point7.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point7.prototype.getDistance = function (another) {
    };
    return Point7;
}());
var point55 = new Point7(1, 4);
point55.x = 5; //Truy cập vào class. Khi property này là public
point55.draw();
//Khi chúng ta cần set các giá trị mà các điều kiện cần phải thỏa mãn thì mới set giá trị cho nó. 
//Nếu ở đâu chung ta set giá trị đều if else thì dư thừa và khó dùng.
//=> Giải pháp cho việc này chúng ta sử dụng getter - setter trong TS
var Point8 = /** @class */ (function () {
    function Point8(_x, _y) {
        this._x = _x;
        this._y = _y;
    }
    Point8.prototype.draw = function () {
        // ...
        console.log('X: ' + this._x + ', Y: ' + this._y);
    };
    Point8.prototype.getDistance = function (another) {
    };
    Object.defineProperty(Point8.prototype, "x", {
        get: function () {
            return this._x;
        },
        set: function (theX) {
            if (theX < 0)
                throw new Error("X cannot be less 0 !");
            this._x = theX;
        },
        enumerable: false,
        configurable: true
    });
    return Point8;
}());
var point8 = new Point8(12, 44);
var x = point8.x;
console.log("Getter:" + x);
point8.draw();
