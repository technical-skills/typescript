var Point5 = /** @class */ (function () {
    function Point5(x, y) {
        this.x = x;
        this.y = y;
    }
    Point5.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point5.prototype.getDistance = function (another) {
    };
    return Point5;
}());
//Có thể chúng ta phải khai báo từng property và gán vào contructor.
// Ở typescipt chúng ta có thể không cần khai báo định nghĩa từng property như v.
//Và còn có thể không cần phải khởi tạo trong contructor. Vì nó sẽ tự gán cho chúng ta.
var Point6 = /** @class */ (function () {
    function Point6(x, y) {
        this.x = x;
        this.y = y;
    }
    Point6.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point6.prototype.getDistance = function (another) {
    };
    return Point6;
}());
var point55 = new Point6(1, 4);
point55.x = 5; //Truy cập vào class. Khi property này là public
point55.draw();
