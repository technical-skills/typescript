class Point5{
    private x: number;
    private y: number;

    constructor(x?: number, y?: number){
        this.x = x;
        this.y = y;
    }
    draw(){
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y );
    }

    getDistance(another : Point5){

    }
}

//Có thể chúng ta phải khai báo từng property và gán vào contructor.
// Ở typescipt chúng ta có thể không cần khai báo định nghĩa từng property như v.

//Và còn có thể không cần phải khởi tạo trong contructor. Vì nó sẽ tự gán cho chúng ta.
class Point6{
    constructor(public x?: number, private y?: number){
    }
    draw(){
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y );
    }

    getDistance(another : Point6){

    }
}

let point55 = new Point6(1,4);
point55.x = 5; //Truy cập vào class. Khi property này là public
point55.draw();

