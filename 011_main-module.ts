//Khai báo từ khóa export để các class khác có thể trỏ đến để dùng class này.
export class Point8{
    constructor(public x?: number, private y?: number){
    }
    draw(){
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y );
    }

    getDistance(another : Point8){

    }
}