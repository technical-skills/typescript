"use strict";
exports.__esModule = true;
exports.Point8 = void 0;
//Khai báo từ khóa export để các class khác có thể trỏ đến để dùng class này.
var Point8 = /** @class */ (function () {
    function Point8(x, y) {
        this.x = x;
        this.y = y;
    }
    Point8.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point8.prototype.getDistance = function (another) {
    };
    return Point8;
}());
exports.Point8 = Point8;
