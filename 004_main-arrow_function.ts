let message : string = "Cuong Nguyen";

//Cách 1: Cách thuần
let log = function(message){
    console.log(message);
}

// Cách 2S: Sử dụng arrow function nhưng vẫn chưa gọn.
let doLog = (message) => {
    console.log(message);
}

//Cách 3: Sử dụng arrow function gọn hơn
let doLog2 = (message) => console.log(message);

//Cách 3.1: Nếu hàm không có tham số thì chúng ta không càn truyền message.
let doLog3 = () => console.log(message);

console.log(log);
console.log(doLog);
console.log(doLog2);
console.log(doLog3);
