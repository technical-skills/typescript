function doSomething3() {
    var number;
    number = true;
    number = 'a';
    number = 7;
    var count = 5;
    console.log(count);
    console.log(number);
}
doSomething3();
// Nếu khi gán giá trị cho biết có khai báo là let - const: thì xác định biến đó thuộc kiểu nào (number, string, boolean)
//-> Thì không thể gán giá trị cho nó bằng kiểu dữ liệu khác được nữa.
// Nhưng khi chuyển sang JS và compile thì tự động chuyển thành var nếu ở ts bị lỗi.
