function doSomething(){
    for (var index = 0; index < 5; index++) {
        console.log(index);
    }
    console.log('Finally: ' + index);
}
doSomething();  

//Nên sử dụng let thay cho var. Vì let sẽ chặt chẽ khi khai báo và sử dụng biến.
//Scope của let const chỉ trong block { }
//Scope của var thì global
/*Lưu ý:
    Khi code ở TypeScript thì khi chuyển sang JS để compile thì các lỗi ở TS cũng tự chuyển thành JS.
*/
function doSomething2(){
    for (let index = 0; index < 5; index++) {
        console.log(index);
    }
    console.log('Finally2 : ' + index);
}
doSomething2();  
