var Point2 = /** @class */ (function () {
    function Point2() {
    }
    Point2.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point2.prototype.getDistance = function (another) {
    };
    return Point2;
}());
/*
let point: Point2;
point.draw();
*/
//Nếu chúng ta chỉ tạo biến là Class thì sẽ bị lỗi vì chúng ta cần phải cấp phát bộ nhớ cho biến theo kiểu Object. 
//Nếu không khai báo cấp phát bộ nhớ thì sẽ bị lỗi.
var point2 = new Point2();
point2.x = 1;
point2.y = 2;
point2.draw();
