var Point3 = /** @class */ (function () {
    function Point3(x, y) {
        this.x = x;
        this.y = y;
    }
    Point3.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point3.prototype.getDistance = function (another) {
    };
    return Point3;
}());
//Chúng ta khởi tạo như thế này và gán biến thì hơi dài dòng. 
//Nếu class có nhiều property thì rất mệt khi code.
/*
let point3: Point3 = new Point3();
point3.x = 1;
point3.y = 2;
point3.draw();
*/
//=> Sinh ra Contructor : Để mọi lớp có thể có một hàm tạo về phương thức khi chúng ta đã tạo 1 thể hiện. 
var point4 = new Point3(1, 2);
point4.draw();
//=> Và đây là kết quả cuối cùng. Khi chúng ta truyền vào 
//X: 1, Y: 2
//NHƯNG: Có những trường hợp chúng ta không truyền khởi tạo lần đầu cho biến thì chúng ta thêm ? sau biến trong contructor.
//Để hiểu được class này khởi tạo new ko cần truyền giá trị.
