var message = 'Nguyen Quoc Cuong';
//Nếu khai báo như vầy thì biến này chắc chắc kiểu dữ liệu là string. Và được hỗ trợ Intellsence để có thể gợi ý các hàm dành cho String.
var endWithC = message.endsWith('C');
console.log(endWithC);
//Cách khai báo kiểu khác.
var message2;
message2 = 'Nguyen Quoc Cuong';
//Lúc này dù gán giá trị là kiểu string nhưng TS hiểu biến này kiểu any
//Cho nên khi chúng ta chấm thì Intellsence không gợi ý các hàm dành cho string được. 
//Ngoài ra khi hệ thống chạy tránh được sự lỗi xảy ra ngoài mong muốn.
var endsWith2 = message2.endsWith('g');
//Hoặc dùng này 
var endsWith3 = message2.endsWith('g');
console.log(endsWith2);
console.log(endsWith3);
