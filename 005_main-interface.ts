//Thường 1 hàm sẽ như thế này.
let drawPoint = (x,y,z,t) => {
    //..do something
}

drawPoint(4,5,6,7);

//Tuy nhiên nếu nhiều parameter > 3 thì nên tạo riêng ra dạng object
let drawPoint2 = (point: {x: number, y: number, z: number, t: number}) => {
    //..do something
}

drawPoint2({
    x: 1,
    y: 2,
    z: 3,
    t: 4
});
//Thay vì mình truyền v thì có thể tạo ra biết object. Rồi truyền vào.
let obj = {
    x: 1,
    y: 2,
    z: 3,
    t: 4
}

//Như trên cũng chưa ổn lắm. Vì truyền object v thì làm đi làm lại rất nhiều lần. Và khó đọc code
interface Point{
    x: number,
    y: number,
    z: number,
    t: number
}

let drawPoint3 = (point: Point) => {
    //..do something
}
let obj2 : Point = {
    x: 1,
    y: 2,
    z: 3,
    t: 4
}
drawPoint3(obj2);

//Thử xem 1 hàm có 2 interface sẽ như thế nào.
let drawPoint4 = (pointA: Point, pointB: Point) => {
    console.log(pointA.x);
    console.log(pointB.x);
}
let obj3 : Point = {
    x: 5,
    y: 2,
    z: 3,
    t: 4
}
drawPoint4(obj2,obj3);

//So sánh Khi nào sử dụng Interface - Class