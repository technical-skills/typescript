//Sử dụng hàng để show các màu của product
var ColorRed = 0;
var ColorGreen = 1;
var ColorBlue = 2;
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
})(Color || (Color = {}));
;
var backgroundColor = Color.Green;
console.log(backgroundColor);
//Thay vì mình gán từng giá trị hằng cho từng biến thì TS có thể dùng Enum
