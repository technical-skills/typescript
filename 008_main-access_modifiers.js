var Point4 = /** @class */ (function () {
    function Point4(x, y) {
        this.x = x;
        this.y = y;
    }
    Point4.prototype.draw = function () {
        // ...
        console.log('X: ' + this.x + ', Y: ' + this.y);
    };
    Point4.prototype.getDistance = function (another) {
    };
    return Point4;
}());
var point5 = new Point4(1, 2);
point5.draw();
