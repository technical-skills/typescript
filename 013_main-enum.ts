//Sử dụng hàng để show các màu của product
const ColorRed = 0;
const ColorGreen = 1;
const ColorBlue = 2;

enum Color{
    Red = 0,
    Green = 1,
    Blue = 2
};

let backgroundColor = Color.Green;
console.log(backgroundColor);

//Thay vì mình gán từng giá trị hằng cho từng biến thì TS có thể dùng Enum để quản lí các Const này.
